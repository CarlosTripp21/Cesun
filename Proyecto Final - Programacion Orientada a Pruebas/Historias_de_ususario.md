Problema Planteado:

Se requiere un sistema de ventas el cual despliegue los productos que el administrador ingrese en el stock disponible. Los administradores ademas podran renovar los inventarios o agregar nuevos productos a la lista. El usuario podra consultar las cantidades, precios, numeros de parte y su debido departamento para verificar que los productos que necesita son los correctos. El sistema debera mostrar el total a pagar al final.

Historias de Usuario:

.- Como administrador, quiero registrar productos nuevos en los inventarios, guardandolos en una base de datos para ser desplegados por el sistema.
.- Como administrador, quiero consultar el inventario actual para revisar el stock en el inventario.
.- Como administrador, quiero saber que productos tengo en bajas cantidades para poder aumentar el stock y evitar productos fuera de stock.
.- Como sistema, quiero mostrar los inventarios en tiempo real, evidandole al cliente mostrar stock falso.
.- Como sistema, quiero mostrar el total de la venta para que el cliente no tenga dudas a la hora de pagar.
.- Como usuario, quiero verificar los productos disponibles, conociendo su costo y localidad dentro de la tienda.
.- Como usuario, quiero poder comprar mas de un solo articulo, ademas de conocer el total de mi compra.